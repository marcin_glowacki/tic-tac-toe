import React, { useCallback, useEffect, useState } from 'react'
import './Body.css'
import Board from './Board'

const Body = () => {

  const [identity, setIdentity] = useState('')
  const [result, setResult] = useState('')
  const [moveDTO, setMoveDTO] = useState({})
  const [board, setBoard] = useState()
  const [localBoard, setLocalBoard] = useState()

  const fetchIdentity = async () => {
    fetch('http://localhost:8080/tic-tac-toe/api/v1/board/identity')
      .then(response => {
        response.text().then(text => {
          setIdentity(text)
        })

      })
      .catch(error => console.log('Error: ', error))
  }
  const fetchBoard = async () => {
    fetch('http://localhost:8080/tic-tac-toe/api/v1/board')
      .then(response => {
        response.json().then(data => {
          setBoard(data)
          setLocalBoard(data)
        })

      })
      .catch(error => console.log('Error: ', error))
  }

  useEffect(() => {
    fetchIdentity()
    fetchBoard()
  }, [])

  const submit = useCallback(
    () => {
      fetch('http://localhost:8080/tic-tac-toe/api/v1/board/submitCommand', {
        method: 'post',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(moveDTO)
      })
        .then(response => {
          response.text().then(text => {

            setResult(text)
            fetchIdentity()
            fetchBoard()
          })
        })
        .catch(error => console.log('Error: ', error))
    },
    [moveDTO]
  )

  const clearBoard = useCallback(
    () => {
      fetch('http://localhost:8080/tic-tac-toe/api/v1/board/clearBoardCommand', { method: 'post' })
        .then(response => {
          fetchBoard()
          setResult('')
        })
        .catch(error => console.log('Error: ', error))
    },
    []
  )

  const localMove = useCallback(
    (rowIndex, cellIndex) => {
      setMoveDTO({ row: rowIndex, column: cellIndex, identity })
      const tempBoard = [[null, null, null], [null, null, null], [null, null, null]]
      for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
          tempBoard[i][j] = board[i][j]
        }
      }
      if (!board[rowIndex][cellIndex] && !result) {
        tempBoard[rowIndex][cellIndex] = identity
        setLocalBoard(tempBoard)
      }
    },
    [board, identity, result]
  )

  return (
    <>
      <h1>Tic Tac Toe</h1>
      <p>Ihre Marke: {identity}</p>
      <Board localBoard={localBoard} identity={identity} callback={localMove}/>
      <button onClick={submit}>Einsenden</button>
      <button onClick={clearBoard}>Neues Spiel</button>
      <p>{result}</p>
    </>
  )
}

export default Body
