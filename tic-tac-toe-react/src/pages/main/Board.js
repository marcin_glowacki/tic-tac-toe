import React from 'react'
import './Board.css'

const Board = ({ localBoard, callback }) => {

  return (
    <table className="board">
      <tbody>
      {
        localBoard ? localBoard.map((row, rowIndex) => <tr key={rowIndex}>{row.map((cell, cellIndex) =>
          <td className={cell} key={cellIndex}
              onClick={() => callback(rowIndex, cellIndex)}>{cell ? cell : ''}</td>)}</tr>) : null
      }
      </tbody>
    </table>
  )
}

export default Board
