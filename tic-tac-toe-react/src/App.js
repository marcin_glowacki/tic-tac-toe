import './App.css'
import Body from './pages/main/Body'

function App () {
  return (
    <div className="App">
      <Body/>
    </div>
  )
}

export default App
