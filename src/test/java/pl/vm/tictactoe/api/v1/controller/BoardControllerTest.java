package pl.vm.tictactoe.api.v1.controller;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
class BoardControllerTest {

    @Test
    public void testBoardEndpoint() {
        given()
                .when()
                .get("/tic-tac-toe/api/v1/board")
                .then()
                .statusCode(200)
                .body(is("[[null,null,null],[null,null,null],[null,null,null]]"));
    }

    @Test
    public void testIdentityEndpoint() {
        given()
                .when()
                .get("/tic-tac-toe/api/v1/board/identity")
                .then()
                .statusCode(200)
                .body(is("X"));
    }

    @Test
    public void testSubmitEndpoint() {
        String moveDTO = "{\"row\":2,\"column\":2,\"identity\":\"O\"}";

        given()
                .contentType(ContentType.JSON)
                .body(moveDTO)
                .post("/tic-tac-toe/api/v1/board/submitCommand")
                .then()
                .statusCode(204);
    }

    @Test
    public void testClearBoardEndpoint() {
        given()
                .contentType(ContentType.JSON)
                .post("/tic-tac-toe/api/v1/board/clearBoardCommand")
                .then()
                .statusCode(200);
    }
}