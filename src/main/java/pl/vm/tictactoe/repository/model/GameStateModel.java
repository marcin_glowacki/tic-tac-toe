package pl.vm.tictactoe.repository.model;

import lombok.*;
import pl.vm.tictactoe.repository.model.common.BaseModel;
import pl.vm.tictactoe.service.dto.Identity;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GameStateModel extends BaseModel {

    private Identity[][] board = new Identity[3][3];
}
