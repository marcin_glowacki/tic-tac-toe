package pl.vm.tictactoe.repository.dao;

import pl.vm.tictactoe.repository.dao.common.BaseDAO;
import pl.vm.tictactoe.repository.model.CurrentAvailableIdentityModel;
import pl.vm.tictactoe.service.dto.Identity;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class CurrentAvailableIdentityDAO extends BaseDAO {

    private final CurrentAvailableIdentityModel CURRENT_AVAILABLE_IDENTITY_MODEL = new CurrentAvailableIdentityModel();

    public Identity getIdentity() {
        Identity currentIdentity = CURRENT_AVAILABLE_IDENTITY_MODEL.getIdentity();
        Identity newIdentity = currentIdentity == null || Identity.O.equals(currentIdentity) ? Identity.X : Identity.O;
        CURRENT_AVAILABLE_IDENTITY_MODEL.setIdentity(newIdentity);

        return newIdentity;
    }
}
