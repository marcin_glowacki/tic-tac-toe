package pl.vm.tictactoe.repository.dao;

import pl.vm.tictactoe.repository.dao.common.BaseDAO;
import pl.vm.tictactoe.repository.model.GameStateModel;
import pl.vm.tictactoe.service.dto.Identity;
import pl.vm.tictactoe.service.dto.move.MoveDTO;

import javax.enterprise.context.ApplicationScoped;
import java.util.Arrays;

@ApplicationScoped
public class GameStateDAO extends BaseDAO {

    private final GameStateModel GAME_STATE_MODEL = new GameStateModel();

    public Identity[][] getBoard() {
        return GAME_STATE_MODEL.getBoard();
    }

    public void setMove(MoveDTO moveDTO) {
        GAME_STATE_MODEL.getBoard()[moveDTO.getRow()][moveDTO.getColumn()] = moveDTO.getIdentity();
    }

    public String whatNext() {
        final Identity[][] board = GAME_STATE_MODEL.getBoard();


        if (xWins(board)) {
            return "X hat gewonnen";
        }
        if (oWins(board)) {
            return "O hat gewonnen";
        }
        if (isDraw(board)) {
            return "Remis";
        }
        return null;
    }

    private boolean xWins(Identity[][] board) {
        return someoneWins(board, Identity.X);
    }

    private boolean oWins(Identity[][] board) {
        return someoneWins(board, Identity.O);
    }

    private boolean someoneWins(Identity[][] board, Identity identity) {
        for (int i = 0; i < 3; i++) {
            if (Arrays.stream(board[i]).allMatch(identity::equals)) {
                return true;
            }
        }
        for (int j = 0; j < 3; j++) {
            boolean isOk = true;
            for (int i = 0; i < 3; i++) {
                if (!identity.equals(board[i][j])) {
                    isOk = false;
                    break;
                }
            }
            if (isOk) {
                return true;
            }
        }
        if (identity.equals(board[1][1])) {
            return (identity.equals(board[0][0]) && identity.equals(board[2][2]))
                    || (identity.equals(board[0][2]) && identity.equals(board[2][0]));
        }
        return false;
    }

    private boolean isDraw(Identity[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == null) {
                    return false;
                }
            }
        }
        return true;
    }

    public void clearBoard() {
        GAME_STATE_MODEL.setBoard(new Identity[3][3]);
    }
}
