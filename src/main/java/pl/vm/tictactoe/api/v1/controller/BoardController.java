package pl.vm.tictactoe.api.v1.controller;

import pl.vm.tictactoe.api.v1.controller.common.BaseController;
import pl.vm.tictactoe.service.BoardService;
import pl.vm.tictactoe.service.dto.Identity;
import pl.vm.tictactoe.service.dto.move.MoveDTO;

import javax.inject.Inject;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/tic-tac-toe/api/v1/board")
public class BoardController extends BaseController {

    @Inject
    BoardService boardService;

    @GET
    @Path("")
    @Consumes(MediaType.WILDCARD)
    @Produces(MediaType.APPLICATION_JSON)
    public String getBoard() {
        return JsonbBuilder.create().toJson(boardService.getBoard());
    }

    @GET
    @Path("/identity")
    @Produces(MediaType.TEXT_PLAIN)
    public Identity getIdentity() {
        return boardService.getIdentity();
    }

    @POST
    @Path("/submitCommand")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String submit(MoveDTO moveDTO) {
        return boardService.submit(moveDTO);
    }

    @POST
    @Path("/clearBoardCommand")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response clearBoard() {
        boardService.clearBoard();
        return Response.ok().build();
    }
}
