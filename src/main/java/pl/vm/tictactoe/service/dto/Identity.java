package pl.vm.tictactoe.service.dto;

public enum Identity {
    X, O
}
