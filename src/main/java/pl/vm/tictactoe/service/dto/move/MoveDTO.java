package pl.vm.tictactoe.service.dto.move;

import lombok.*;
import pl.vm.tictactoe.service.dto.Identity;
import pl.vm.tictactoe.service.dto.common.BaseDTO;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MoveDTO extends BaseDTO {

    private int row;
    private int column;
    private Identity identity;
}
