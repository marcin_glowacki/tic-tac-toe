package pl.vm.tictactoe.service;

import pl.vm.tictactoe.repository.dao.CurrentAvailableIdentityDAO;
import pl.vm.tictactoe.repository.dao.GameStateDAO;
import pl.vm.tictactoe.service.common.BaseService;
import pl.vm.tictactoe.service.dto.Identity;
import pl.vm.tictactoe.service.dto.move.MoveDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class BoardService extends BaseService {

    @Inject
    GameStateDAO gameStateDAO;

    @Inject
    CurrentAvailableIdentityDAO currentAvailableIdentityDAO;

    public Identity[][] getBoard() {
        return gameStateDAO.getBoard();
    }

    public Identity getIdentity() {
        return currentAvailableIdentityDAO.getIdentity();
    }

    public String submit(MoveDTO moveDTO) {
        gameStateDAO.setMove(moveDTO);
        return gameStateDAO.whatNext();
    }

    public void clearBoard() {
        gameStateDAO.clearBoard();
    }
}
